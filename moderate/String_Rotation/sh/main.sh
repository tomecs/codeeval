#!/bin/bash


INPUT_FILE=$1
declare -a startCharPositions


lineParsing() 
{
	basicWord=$(cut -d ',' -f1 <<<"${1}")
	rotatedWord=$(cut -d ',' -f2 <<<"${1}")	
}

wordsToArray() 
{
	read -r -a arrayBasicWord <<< $(grep -o . <<< ${basicWord})
	read -r -a arrayRotatedWord <<< $(grep -o . <<< ${rotatedWord})

#	for (( i=0; i<${#basicWord}; i++ ))
#	do
#		arrayBasicWord=${basicWord:$i:1}
#		arrayRotatedWord=${rotatedWord:$i:1}
#	done
}

checkEndOfWord() 
{

	if [ $indexOnRotated -eq ${#arrayRotatedWord[@]} ]
	then
		indexOnRotated=0
	fi

}

searchPossibleStartChar() 
{

	startChar=${arrayBasicWord[0]}	
	index=0	
	startCharPositions=()

	for (( i=0; i<${#arrayRotatedWord[@]}; i++ ))
	do
		if [ ${arrayRotatedWord[$i]} == $startChar ]
		then
			startCharPositions[${index}]=$i
			(( index++ ))
		fi

	done
}

compareChars() 
{
		
	finished=false
	attemptFirstPos=0

	
	if [ ${#arrayBasicWord[@]} -eq ${#arrayRotatedWord[@]} ]
	then
	
		while [ $attemptFirstPos -lt ${#startCharPositions[@]} ] 
		do
			counter=${#arrayBasicWord[@]}
			indexOnBasic=0						 
			indexOnRotated=${startCharPositions[$attemptFirstPos]}
			(( attemptFirstPos++ ))

			for (( i=0; i<${#arrayRotatedWord[@]}; i++ ))			
			do
				if [ ${arrayRotatedWord[$indexOnRotated]} == ${arrayBasicWord[$indexOnBasic]} ]
				then 
					(( indexOnRotated++ ))
					(( indexOnBasic++ ))
					(( counter-- ))
		
					checkEndOfWord		
				fi

				if [ $counter -eq 0 ]
				then
					finished=true
				fi			
			done

		done
	fi

	if [ $finished == true ]
	then 
		echo True
	else
		echo False
	fi
	
}




main() 
{

	while read -r line
	do 
		lineParsing "${line}"
		wordsToArray
		searchPossibleStartChar
		compareChars
		

	done < ${INPUT_FILE}

}

main





#!/bin/sh

INPUT_FILE=$1




LineParsing() {
	word=$(cut -d ' ' -f1 <<<"${1}")
	mask=$(cut -d ' ' -f2 <<<"${1}")
}

WordMasking() {
	
	resultWord=""
	for (( i=0; i<${#word}; i++ ))
	do
		char=${word:$i:1}
		maskDigit=${mask:$i:1}
		
		if [ $maskDigit -eq 1 ]
		then
			resultWord="${resultWord}$(tr '[:lower:]' '[:upper:]' <<< $char)"
		else
			resultWord="${resultWord}$char"
		fi
	done

}



while read -r line
do 
	LineParsing "${line}"	

	WordMasking
	echo $resultWord


done < "${INPUT_FILE}"


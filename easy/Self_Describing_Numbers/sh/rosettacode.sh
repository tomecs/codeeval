#!/bin/bash

selfdescribing() {
    local n=$1
    local count=()
    local i
    for ((i=0; i<${#n}; i++)); do
        ((count[${n:i:1}]++))
    done
    for ((i=0; i<${#n}; i++)); do
        (( ${n:i:1} == ${count[i]:-0} )) || return 1
    done
    return 0
}

numbers=(2020
42101000
22
21200
3211000
1210
681729
944742
420062
75514
266741
333080
846264
214700
525288
440988
196953
614921
549463
312296
65963
784592
662583
3566
707971
645503) 

for n in "${numbers[@]}"; do 
    if selfdescribing $n; then
        printf "%d\t%s\n" $n yes
    else
        printf "%d\t%s\n" $n no
    fi
done





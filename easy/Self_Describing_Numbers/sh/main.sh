#!/bin/bash

INPUT_FILE=$1


while read -r lineOfFile
do
	declare digitArray

	i=0
	while read charOfLine
	do 		
		digitArray[$i]=$charOfLine
		((i+=1))
	done < <(grep -o . <<< "${lineOfFile}")

	i=0

	while [ ${i} -lt ${#digitArray[@]} ]
	do
		counter=0		
		j=${digitArray[$i]}

		while read -r charOfLine		
		do
			if [ $charOfLine == $i ]
			then 
				((counter+=1))
			fi	
		done < <(grep -o . <<< "${lineOfFile}")
		
		if [ $counter -ne $j ]
		then
			echo 0 
			break
		elif [ $i -eq $((${#digitArray[@]}-1)) ]
		then
			echo 1 
		fi
			
		((i+=1))
	done
	
done < $INPUT_FILE





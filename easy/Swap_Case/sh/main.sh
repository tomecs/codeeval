#!/bin/bash

INPUT_FILE=$1
OLDIFS=$IFS
IFS=$'\n' 


LowerToUpper() {
	tmp=$(tr "[:lower:]" "[:upper:]" <<<"${char}")
	result="${result}${tmp}"
}

UpperToLower() {
	tmp=$(tr "[:upper:]" "[:lower:]" <<<"${char}")
	result="${result}${tmp}"
}

LineProcessing() {
	while read -r char
	do
		if grep -q [a-z]<<<"${char}" 
		then
			LowerToUpper	
		elif grep -q [A-Z]<<<"${char}"
		then			
			UpperToLower
		else
			result="${result}${char}"
		fi
	done < <(grep -o . <<< "${line}")
}


while read -r line
do 
	result=""
	LineProcessing

	echo $result

done < $INPUT_FILE

IFS=$OLDIFS





